import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/compat/firestore";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private afs: AngularFirestore) { }

  addDoctor(doctor: any) {
    doctor.id = this.afs.createId();
    return this.afs.collection("doctor/").add(doctor);
  }

  getAllDoctor() {
    return this.afs.collection("doctor/").snapshotChanges();
  }
  updateDoctor(doctor : any) {
    return this.afs.doc("doctor/"+doctor.id).update(doctor);
  }

  deleteDoctor(id : string) {
    return this.afs.doc("doctor/"+id).delete();
  }

  getDoctorById(id : string){
    return this.afs.doc("doctor/"+id).valueChanges();
  }

  addPatient(patient: any) {
    patient.patient_id = this.afs.createId();
    return this.afs.collection("patient/").add(patient);
  }

  getAllPatient() {
    return this.afs.collection("patient/").snapshotChanges();
  }

  updatePatient(patient : any) {
    return this.afs.doc("patient/"+patient.patient_id).update(patient);
  }

  deletePatient(id : string) {
    return this.afs.doc("patient/"+id).delete();
  }

  getPatientById(id : any) {
    return this.afs.doc("patient/"+id).valueChanges();
  }

}

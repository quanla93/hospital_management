import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../../shared/service/auth.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form !: FormGroup;
  email: any = '';
  password: any = '';

  constructor(private authApi : AuthService,
              private fb : FormBuilder) {
    this.form = this.fb.group({
      email: [this.email, [Validators.email, Validators.required]],
      password: [this.password, Validators.required],

    })
  }

  ngOnInit(): void {
  }

  login() {
    this.authApi.login(this.form.value.email, this.form.value.password)
  }
}
